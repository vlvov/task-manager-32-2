package ru.t1.vlvov.tm.dto.Response;

import ru.t1.vlvov.tm.model.Task;

public final class TaskRemoveByIdResponse extends AbstractTaskResponse {

    public TaskRemoveByIdResponse(Task task) {
        super(task);
    }

}
