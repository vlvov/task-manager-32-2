package ru.t1.vlvov.tm.dto.Response;

import ru.t1.vlvov.tm.model.User;

public final class UserUpdateProfileResponse extends AbstractUserResponse {

    public UserUpdateProfileResponse(User user) {
        super(user);
    }
}
