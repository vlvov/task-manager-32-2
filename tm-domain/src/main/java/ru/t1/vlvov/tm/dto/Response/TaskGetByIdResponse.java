package ru.t1.vlvov.tm.dto.Response;

import ru.t1.vlvov.tm.model.Task;

public final class TaskGetByIdResponse extends AbstractTaskResponse {

    public TaskGetByIdResponse(Task task) {
        super(task);
    }

}
