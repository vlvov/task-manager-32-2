package ru.t1.vlvov.tm.dto.Response;

import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.model.User;

public final class UserRegistryResponse extends AbstractUserResponse {

    public UserRegistryResponse(@Nullable final User user) {
        super(user);
    }

}
