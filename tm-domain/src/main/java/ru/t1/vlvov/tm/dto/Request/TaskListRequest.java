package ru.t1.vlvov.tm.dto.Request;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.enumerated.Sort;

@Getter
@Setter
public final class TaskListRequest extends AbstractUserRequest {

    @Nullable
    private Sort sort;

}
