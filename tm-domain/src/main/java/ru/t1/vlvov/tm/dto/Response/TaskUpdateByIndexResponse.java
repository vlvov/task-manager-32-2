package ru.t1.vlvov.tm.dto.Response;

import ru.t1.vlvov.tm.model.Task;

public class TaskUpdateByIndexResponse extends AbstractTaskResponse {

    public TaskUpdateByIndexResponse(Task task) {
        super(task);
    }

}
