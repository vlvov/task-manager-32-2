package ru.t1.vlvov.tm.dto.Request;

import org.jetbrains.annotations.Nullable;

public final class UserLogoutRequest extends AbstractUserRequest {

    @Nullable
    private String login;

}
