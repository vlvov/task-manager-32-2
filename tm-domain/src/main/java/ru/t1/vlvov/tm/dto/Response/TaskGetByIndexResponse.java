package ru.t1.vlvov.tm.dto.Response;

import ru.t1.vlvov.tm.model.Task;

public final class TaskGetByIndexResponse extends AbstractTaskResponse {

    public TaskGetByIndexResponse(Task task) {
        super(task);
    }

}
