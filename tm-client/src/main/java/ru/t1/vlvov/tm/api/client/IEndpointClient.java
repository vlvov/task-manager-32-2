package ru.t1.vlvov.tm.api.client;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.dto.Request.AbstractRequest;
import ru.t1.vlvov.tm.dto.Response.ApplicationErrorResponse;

import java.io.*;
import java.net.Socket;

public interface IEndpointClient {

    @NotNull
    Object call(@NotNull final Object object) throws IOException, ClassNotFoundException;

    @Nullable
    Socket connect() throws IOException;

    @Nullable
    Socket disconnect() throws IOException;

    @NotNull
    <T> T call(@Nullable final AbstractRequest request, @NotNull Class<T> classResponse) throws IOException, ClassNotFoundException;

}
