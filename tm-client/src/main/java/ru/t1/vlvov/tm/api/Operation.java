package ru.t1.vlvov.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.t1.vlvov.tm.dto.Request.AbstractRequest;
import ru.t1.vlvov.tm.dto.Response.AbstractResponse;

@FunctionalInterface
public interface Operation<RQ extends AbstractRequest, RS extends AbstractResponse> {

    @NotNull
    RS execute(@NotNull RQ request);

}
