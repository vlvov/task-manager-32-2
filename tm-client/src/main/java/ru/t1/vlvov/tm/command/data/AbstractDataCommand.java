package ru.t1.vlvov.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.client.DomainEndpointClient;
import ru.t1.vlvov.tm.client.ProjectEndpointClient;
import ru.t1.vlvov.tm.command.AbstractCommand;
import ru.t1.vlvov.tm.dto.Domain;
import ru.t1.vlvov.tm.enumerated.Role;
import ru.t1.vlvov.tm.exception.system.ServiceNotFoundException;

public abstract class AbstractDataCommand extends AbstractCommand {

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @Override
    public @Nullable Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @NotNull
    protected DomainEndpointClient getDomainEndpointClient() {
        return serviceLocator.getDomainEndpointClient();
    }

}
