package ru.t1.vlvov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.dto.Request.ProjectGetByIndexRequest;
import ru.t1.vlvov.tm.dto.Request.TaskGetByIndexRequest;
import ru.t1.vlvov.tm.model.Project;
import ru.t1.vlvov.tm.model.Task;
import ru.t1.vlvov.tm.util.TerminalUtil;

public final class TaskShowByIndexCommand extends AbstractTaskCommand {

    @NotNull
    private final String DESCRIPTION = "Show task by Index.";

    @NotNull
    private final String NAME = "task-show-by-index";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @Nullable final Integer index = TerminalUtil.nextNumber() - 1;
        @Nullable final TaskGetByIndexRequest request = new TaskGetByIndexRequest();
        request.setTaskIndex(index);
        @Nullable final Task task = getTaskEndpointClient().getByIndex(request).getTask();
        showTask(task);
    }

}