package ru.t1.vlvov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.dto.Request.ProjectGetByIdRequest;
import ru.t1.vlvov.tm.dto.Request.ProjectGetByIndexRequest;
import ru.t1.vlvov.tm.model.Project;
import ru.t1.vlvov.tm.util.TerminalUtil;

public final class ProjectShowByIndexCommand extends AbstractProjectCommand {

    @NotNull
    private final String DESCRIPTION = "Show project by Index.";

    @NotNull
    private final String NAME = "project-show-by-index";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @Nullable final Integer index = TerminalUtil.nextNumber() - 1;
        @Nullable final ProjectGetByIndexRequest request = new ProjectGetByIndexRequest();
        request.setIndex(index);
        @Nullable final Project project = getProjectEndpointClient().getByIndex(request).getProject();
        showProject(project);
    }

}