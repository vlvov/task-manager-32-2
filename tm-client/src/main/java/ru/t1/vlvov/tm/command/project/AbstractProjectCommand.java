package ru.t1.vlvov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.client.ProjectEndpointClient;
import ru.t1.vlvov.tm.command.AbstractCommand;
import ru.t1.vlvov.tm.enumerated.Role;
import ru.t1.vlvov.tm.enumerated.Status;
import ru.t1.vlvov.tm.exception.system.ServiceNotFoundException;
import ru.t1.vlvov.tm.model.Project;

import java.nio.file.ProviderNotFoundException;

public abstract class AbstractProjectCommand extends AbstractCommand {

    @NotNull
    protected ProjectEndpointClient getProjectEndpointClient() {
        if (serviceLocator == null) throw new ServiceNotFoundException();
        return serviceLocator.getProjectEndpointClient();
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    protected void showProject(@Nullable final Project project) {
        if (project == null) throw new ProviderNotFoundException();
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + Status.toName(project.getStatus()));
        System.out.println("USER ID: " + project.getUserId());
    }

}
