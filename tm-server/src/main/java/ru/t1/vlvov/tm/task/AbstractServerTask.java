package ru.t1.vlvov.tm.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.vlvov.tm.component.Server;

public abstract class AbstractServerTask implements Runnable {

    @NotNull
    protected final Server server;

    public AbstractServerTask(@NotNull final Server server) {
        this.server = server;
    }

}
