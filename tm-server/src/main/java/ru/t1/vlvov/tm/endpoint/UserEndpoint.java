package ru.t1.vlvov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.api.endpoint.IUserEndpoint;
import ru.t1.vlvov.tm.api.service.IServiceLocator;
import ru.t1.vlvov.tm.dto.Request.*;
import ru.t1.vlvov.tm.dto.Response.*;
import ru.t1.vlvov.tm.model.User;

public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public @NotNull UserChangePasswordResponse changeUserPassword(@NotNull UserChangePasswordRequest request) {
        @Nullable final String userId = request.getUserId();
        @Nullable final String password = request.getPassword();
        @NotNull final User user = serviceLocator.getUserService().setPassword(userId, password);
        return new UserChangePasswordResponse(user);
    }

    @Override
    public @NotNull UserLockResponse lockUser(@NotNull UserLockRequest request) {
        @Nullable final String login = request.getLogin();
        @NotNull final User user = serviceLocator.getUserService().lockUserByLogin(login);
        return new UserLockResponse(user);
    }

    @Override
    public @NotNull UserUnlockResponse unlockUser(@NotNull UserUnlockRequest request) {
        @Nullable final String login = request.getLogin();
        @NotNull final User user = serviceLocator.getUserService().unlockUserByLogin(login);
        return new UserUnlockResponse(user);
    }

    @Override
    public @NotNull UserRemoveResponse removeUser(@NotNull UserRemoveRequest request) {
        @Nullable final String login = request.getLogin();
        @NotNull final User user = serviceLocator.getUserService().removeByLogin(login);
        return new UserRemoveResponse(user);
    }

    @Override
    public @NotNull UserUpdateProfileResponse updateUserProfile(@NotNull UserUpdateProfileRequest request) {
        @Nullable final String userId = request.getUserId();
        @Nullable final String firstName = request.getFirstName();
        @Nullable final String lastName = request.getLastName();
        @Nullable final String middleName = request.getMiddleName();
        @NotNull final User user = serviceLocator.getUserService().updateUser(userId, firstName, lastName, middleName);
        return new UserUpdateProfileResponse(user);
    }

    @Override
    public @NotNull UserRegistryResponse registryUser(@NotNull UserRegistryRequest request) {
        @Nullable final String login = request.getLogin();
        @Nullable final String password = request.getPassword();
        @Nullable final String email = request.getEmail();
        @NotNull final User user = serviceLocator.getAuthService().registry(login, password, email);
        return new UserRegistryResponse(user);
    }

}
